import 'package:f290_ddm2_sqlite_contacts/helper/sqlite_helper.dart';

class ContatoService {
  var helper = SQLiteOpenHelper();

  Future inserirContato(Contato c) async {
    try {
      var contato = await helper.insert(c);
      print('Contato inserido com sucesso. Id: ${contato.id}');
    } catch (e) {
      print('Erro ao persistir dados na entidade. Ex: ${e.toString()}');
    }
  }
}
