import 'package:f290_ddm2_sqlite_contacts/screens/adicionar_contato.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: AdicionarContato(),
    );
  }
}
