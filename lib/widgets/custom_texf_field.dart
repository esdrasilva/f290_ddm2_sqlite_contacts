import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String hint;
  final String label;
  final IconData iconData;
  final TextEditingController controller;

  const CustomTextField(
      {this.hint, this.label, this.iconData, this.controller});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
            prefixIcon: Icon(iconData ?? Icons.text_fields),
            hintText: hint ?? '',
            labelText: label ?? '',
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.teal.shade200),
                borderRadius: BorderRadius.all(Radius.elliptical(5, 5)))),
      ),
    );
  }
}
