import 'package:f290_ddm2_sqlite_contacts/helper/sqlite_helper.dart';
import 'package:f290_ddm2_sqlite_contacts/service/contato_service.dart';
import 'package:f290_ddm2_sqlite_contacts/widgets/custom_texf_field.dart';
import 'package:flutter/material.dart';

class AdicionarContato extends StatefulWidget {
  @override
  _AdicionarContatoState createState() => _AdicionarContatoState();
}

class _AdicionarContatoState extends State<AdicionarContato> {
  var service = ContatoService();
  var controllerNome = TextEditingController();
  var controllerEmail = TextEditingController();
  var controllerTelefone = TextEditingController();

  @override
  void initState() {
    var helperSqlite = SQLiteOpenHelper();
    helperSqlite.findAll().then((list) {
      list.forEach((element) {
        print(element);
      });
    }).catchError((onError) => print(
        'Erro ao recuperar dados no banco de dados. Ex: ${onError.toString()}'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Adicionar Contatos')),
      body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
        SizedBox(
          height: 32,
        ),
        Container(
          width: 150,
          height: 150,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: AssetImage('images/images_social.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        CustomTextField(
          controller: controllerNome,
          hint: 'Nome',
          label: 'Nome',
          iconData: Icons.text_fields,
        ),
        CustomTextField(
          controller: controllerEmail,
          hint: 'E-mail',
          label: 'E-mail',
          iconData: Icons.email,
        ),
        CustomTextField(
          controller: controllerTelefone,
          hint: 'Telefone',
          label: 'Telefone',
          iconData: Icons.phone_android,
        ),
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          var contato = Contato(
              nome: controllerNome.text,
              email: controllerEmail.text,
              telefone: controllerTelefone.text);
          service.inserirContato(contato);
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
